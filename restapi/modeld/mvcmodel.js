const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const validatorr = require('validator');

//make a schema

const makeschema = new Schema({
    name:{
        type:String,
        required:true
    },
    roll :{
        type:String,
    },
    email: {
        type:String,
        validate:{
            validator: (v)=>{
                return validatorr.isEmail(v);
            },
            message: "{VALUE} is not valid gmail"
        }
    },
    varsity:
    {
        type:String,
        minlength:5
    }

});

//make model now
const makemodel_mvc = mongoose.model('MVC_model', makeschema); //MVC_model is created to mongo object

module.exports = makemodel_mvc;

