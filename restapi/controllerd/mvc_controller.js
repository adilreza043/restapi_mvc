const mvc_model = require('../modeld/mvcmodel');

const inserData = (req, resp, next)=>{
    const temp_object = new mvc_model({
        name: req.body.name,
        roll: req.body.roll,
        email: req.body.email,
        varsity: req.body.varsity
    });
    temp_object.save()
                .then(data=>{
                    resp.json({
                       // data,
                        "message": "Huhu data is inserted to database"
                    })
                })
                .catch(err=>{
                    resp.json({
                        err,
                        "message": "sorry something is wrong"
                    })
                });
}

const showData = (req, resp,next)=>{
    mvc_model.find()
             .then(alldatabaseResult=>{
                 resp.json({
                    alldatabaseResult
                 })
             })

}

const singledata_finder=(req,resp,next)=>{
    let id=req.params.id;
    //resp.send(id);
    mvc_model.findById(id)
             .then(result=>{
                 //resp.send("id found");
                 resp.json({
                     result
                 })
             })
             .catch(err=>{
                 resp.send("Id not found");
             })
}
const singledata_delete=(req,resp, next)=>{
    let id = req.params.id;
    mvc_model.findByIdAndRemove(id)
            .then(result=>{
                //resp.send("succeess fully removed");
                resp.json({
                    result,
                    "message":"ok that removed"
                })

            })
            .catch(err=>{
                resp.send("sorry id not found or not deleted")
            })
}

const singledata_update=(req,resp,next)=>{
    let id = req.params.id
    resp.send(id);
    // let  updatedata = new mvc_model({
    //     id: "5bcb18b48311e82ec8999ccd",
    //     name: "adil update",
    //     roll: "updated roll",
    //     email: "updatedmail@gmail.com",
    //     varsity: "ruet technology"
    // })

    // mvc_model.findByIdAndUpdate(id, { $set: updatedata })
    //         .then(result=>{
    //             resp.json({
    //                 result,
    //                 'message':"updated data successfully"
    //             })
    //         })
    //         .catch(err=>{
    //             resp.send(err);
    //         })
}

module.exports = {
    inserData,
    showData,
    singledata_finder,
    singledata_delete,
    singledata_update
}