const express = require('express');
const routerboss = express.Router();

//include model
const mvcmodel_object = require('../modeld/mvcmodel');
//end of including model

// routerboss.get('/',(req, resp, next)=>{
//     resp.send("This is get test");
// })

//CCCCCCCCCCCCCCCCCCC ~_~ controller importing --------CCCCCCCCCCC
const mvc_controller = require('../controllerd/mvc_controller');
//end of controller importing

routerboss.post('/', mvc_controller.inserData);
routerboss.get('/', mvc_controller.showData);
routerboss.get('/:id', mvc_controller.singledata_finder);
routerboss.delete('/:id', mvc_controller.singledata_delete);
routerboss.put('/:id', mvc_controller.singledata_update);



module.exports = routerboss;